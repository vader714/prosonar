package com.example.demo.User.controller;


import com.example.demo.User.dto.UserDto;
import com.example.demo.User.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("user")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/create")
    public String create(@RequestBody UserDto userDto) {

        this.userService.saveCustomer(userDto);
        return "guardado";
    }

    @GetMapping("getAll")
    public List<UserDto> getAll(){
        return userService.getUsers();
    }

    @GetMapping("getUserById/{id}")
    public Optional<UserDto> getUserById(@PathVariable("id") Long id){
        return userService.getUserById(id);
    }

    @GetMapping("deleteUserById/{id}")
    public String deleteUserById(@PathVariable("id") Long id){
        return userService.deleteUserById(id);
    }

    @PostMapping("updateUser")
    public String deleteUserById(@RequestBody UserDto userDto){
        return userService.updateUser(userDto);
    }
}
